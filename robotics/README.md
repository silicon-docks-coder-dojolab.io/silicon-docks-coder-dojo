# Robotics projects
Let's share ideas and information about Arduino (developed by Italians! :-D) and components which could be useful for CoderDojo (Senior and Junior)!

## About Robotics
The [Arduino starter kit](https://www.arduino.cc/en/Main/ArduinoStarterKit) which is available @TheDock.

## Links of interest
Simulator for Arduino: [Circuit.io](https://circuits.io/).

### Ideas
DC Motor Robots:
* https://www.youtube.com/watch?v=Sdna5gaTZHA <p/>
* https://www.youtube.com/watch?v=wLO263wd-v4 <p/>
* https://www.youtube.com/watch?v=Z7N0xCDVzIA <p/>

Games:
* https://www.youtube.com/watch?v=VPrhFJmVRyM <p/>
* https://www.youtube.com/watch?v=ZuAdivxIxCY <p/> 
* https://www.youtube.com/watch?v=1jckzxHWlmU <p/> 
* https://www.youtube.com/watch?v=4mJC-9UPDFw&t=0s <p/>

### Sketches
TODO


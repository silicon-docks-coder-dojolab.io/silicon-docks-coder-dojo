# Repo for sharing stuff for C0d3rD0j0

## 1. Getting started

  * If you're in windows you can use our pre-packaged [set of tools](https://drive.google.com/open?id=0B7oQF9viGPDNTGdKZ1BIVVBxUHc).

  * Otherwise you will need just a text editor such as [notepad++](https://notepad-plus-plus.org/download/v6.9.2.html) and a web browser


## 2. Examples

  * ### 2.1.JavaScript

    * The [Simple JS example](JS example/basic-html-with-js.html) is recommended for white belt ninjas

    * The [gravity-game](JS example/gravity-game.html) is recommended only for grand master ninjas!


  * ### 2.2.Raspberry Pi

    * See the raspberry pi [readme](raspberry/README.md)! It's full of awesome!

## 3. Links of interest

  * Go to the [CoderDojo home page](https://coderdojo.com/) for more info

  * ### 3.1 JavaScript

    * [W3C](http://www.w3schools.com/games/) is always the first stop

    * Follow [this](https://developer.mozilla.org/en-US/docs/Games/Tutorials/2D_Breakout_game_pure_JavaScript#Next_steps) 2D game part breakdown to learn the different parts of a game

## 4.Content

  * ### 4.1.January 16th

      [Week One Introduction](SeniorDojoIntro.pptx)
      <br>
      [HTML Cheat sheet](https://websitesetup.org/html5-cheat-sheet/)

  * ### 4.2.January 30th

    [Introduction to Raspberry Pi](https://gitlab.com/silicon-docks-coder-dojo.gitlab.io/silicon-docks-coder-dojo/blob/master/raspberry/SeniorDojoPi1.pptx)

  * ### 4.3.February 12th

    [Javascript Snake Game](https://gitlab.com/silicon-docks-coder-dojo.gitlab.io/silicon-docks-coder-dojo/tree/master/JS%20example/snake-game)

  * ### 4.4.February 27th

    Control snake game using Python & Raspberry Pi
    
    [Pi Control using joystick] (https://gitlab.com/silicon-docks-coder-dojo.gitlab.io/silicon-docks-coder-dojo/blob/master/raspberry/sense-input.TODO.py)
    
    [Pi Control using accelerometer] (https://gitlab.com/silicon-docks-coder-dojo.gitlab.io/silicon-docks-coder-dojo/blob/master/raspberry/sense-input2.TODO.py)


  * ### 4.5 March 13th
 
    Raspberry Pi and Capacitive Touch

    [Raspberry Pi3 GPIO](https://gitlab.com/silicon-docks-coder-dojo.gitlab.io/silicon-docks-coder-dojo/blob/master/raspberry/pi3_gpio.png.pdf)
    
    [Capacitive Touch](https://gitlab.com/silicon-docks-coder-dojo.gitlab.io/silicon-docks-coder-dojo/blob/master/raspberry/touch-keyboard-TODO.py)
    
    
  * ### 4.6 March 27th
 
    [Project Ideas Website](https://gitlab.com/silicon-docks-coder-dojo.gitlab.io/silicon-docks-coder-dojo/tree/master/Website)
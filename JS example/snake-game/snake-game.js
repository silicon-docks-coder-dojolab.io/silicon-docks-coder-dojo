// 1. Create snake
// 2. Create food
// 3. Control the snake with your keyboard

$(document).ready(function() {
    //Canvas stuff
    var canvas = $("#canvas")[0];
    var ctx = canvas.getContext("2d");
    var width = $("#canvas").width();
    var height = $("#canvas").height();

    // variables
  	var size = 10; //size of snake
  	var direction; //direction
  	var food;
  	var score;
  	var speed = 90; //speed of snake
  	//snake
  	var snake_array;

    function init() {
        direction = "right"; //default direction
        create_snake();
        create_food();
        //display the score
        score = 0;

        if (typeof game_loop != "undefined")
            clearInterval(game_loop);
        game_loop = setInterval(paint, speed);
    }
    init();

    function create_snake() {
      //write code to create snake here
        var length = 10; //Length of the snake

        snake_array = []; //Empty array to start with

        for (var i = length - 1; i >= 0; i--) {
            //This will create a horizontal snake starting from the top left
            snake_array.push({x: i, y: 0});
        }

    }

    function create_food() {
      //write code to create food here
        food = {

            x: Math.round(Math.random() * (width - size) / size),
            y: Math.round(Math.random() * (height - size) / size)

        };

        //This will create a cell with x/y between 0-44
        //Because there are 45(450/10) positions accross the rows and columns
    }

    //Lets paint the snake now
    function paint() {
        ctx.fillStyle = "white"; //changes canvas color
        ctx.fillRect(0, 0, width, height);
        ctx.strokeStyle = "black"; //changes canvas border color
        ctx.strokeRect(0, 0, width, height);

        //Move the snake - Pop out the tail cell and place it infront of the head cell
        var nx = snake_array[0].x;
        var ny = snake_array[0].y;

        //We will increment it to get the new head position
        //Lets add proper direction based movement now
        if (direction == "right") nx++;
        else if (direction == "left") nx--;
        else if (direction == "up") ny--;
        else if (direction == "down") ny++;

        //This will restart the game if the snake hits the wall
        //Now if the head of the snake bumps into its body, the game will restart
        if (nx == -1 || nx == width / size || ny == -1 || ny == height / size || check_collision(nx, ny, snake_array)) {
            //restart game
            init();
            return;
        }

        //Lets write the code to make the snake eat the food
        //If the new head position matches with that of the food, Create a new head instead of moving the tail
        if (nx == food.x && ny == food.y) {
            var tail = {x: nx, y: ny};
            score++;
            //Create new food
            create_food();
        } else {
            var tail = snake_array.pop(); //pops out the last cell
            tail.x = nx;
            tail.y = ny;
        }
        //The snake can now eat the food.

        snake_array.unshift(tail); //puts back the tail as the first cell

        for (var i = 0; i < snake_array.length; i++) {
            var c = snake_array[i];
            //Lets paint 10px wide cells
            paint_cell(c.x, c.y);
        }

        //Lets paint the food
        paint_cell(food.x, food.y);

        //Lets paint the score
        var score_text = "Score: " + score; //can change text at bottom here
        ctx.fillText(score_text, 5, height - 5);
    }

    //Lets first create a generic function to paint cells of snake
    function paint_cell(x, y) {
        ctx.fillStyle = "black"; //change color of snake and food
        ctx.fillRect(x * size, y * size, size, size);
        ctx.strokeStyle = "white";
        ctx.strokeRect(x * size, y * size, size, size);
    }

    function check_collision(x, y, array) {
        //This function will check if the provided x/y coordinates exist
        //in an array of cells or not
        for (var i = 0; i < array.length; i++) {
            if (array[i].x == x && array[i].y == y)
                return true;
            }
        return false;
    }

    //Lets add the keyboard controls now
    $(document).keydown(function(e) {
        var key = e.which;
        //write your code to make the snake keyboad controllable
        //ie: right, down, left, up will move the snake
        if (key == "37" && direction != "right")
            direction = "left";
        else if (key == "38" && direction != "down")
            direction = "up";
        else if (key == "39" && direction != "left")
            direction = "right";
        else if (key == "40" && direction != "up")
            direction = "down";
            //The snake is now keyboard controllable
        }
    )
})

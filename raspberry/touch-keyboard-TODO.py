import atexit
import logging
import subprocess
import sys
import time

import Adafruit_MPR121.MPR121 as MPR121
import RPi.GPIO as GPIO
import uinput


# Define mapping of capacitive touch pin presses to keyboard button presses.
KEY_MAPPING = {
                0: uinput.KEY_UP,
                1: uinput.#SOMETHING MISSING HERE!#,
              }

IRQ_PIN = 26

# Don't change the below values unless you know what you're doing.  These help
# adjust the load on the CPU vs. responsiveness of the key detection.
MAX_EVENT_WAIT_SECONDS = 0.5
EVENT_WAIT_SLEEP_SECONDS = 0.1

subprocess.check_call(['modprobe', 'uinput'])

device = uinput.Device(KEY_MAPPING.values())

cap = MPR121.MPR121()
if not cap.begin():
    print('Failed to initialize MPR121, check your wiring!')
    sys.exit(1)

GPIO.setmode(GPIO.BCM)
GPIO.setup(IRQ_PIN, GPIO.IN, pull_up_down=GPIO.PUD_UP)
GPIO.add_event_detect(IRQ_PIN, GPIO.FALLING)
atexit.register(GPIO.cleanup)

cap.touched()

print('Press Ctrl-C to quit.')
while True:
    start = time.time()
    while (time.time() - start) < MAX_EVENT_WAIT_SECONDS and not GPIO.event_detected(IRQ_PIN):
        time.sleep(EVENT_WAIT_SLEEP_SECONDS)
    touched = cap.touched()
    for pin, key in #sometjing missing here!#.iteritems():
        pin_bit = 1 << pin
        if touched & pin_bit:
            logging.debug('Input {0} touched.'.format(pin))
            device.emit_click(key)

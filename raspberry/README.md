# Raspberry Pi projects

## About the rapsberry pi
A really cheap but full fledged computer. Latest model has embedded WIFI and bluetooth.
See [Raspberry Pi Hardware Guide](https://www.raspberrypi.org/learning/hardware-guide/components/raspberry-pi/) for more info.

## Links of interest
https://www.raspberrypi.org/resources/make/

### Ideas
https://www.youtube.com/watch?v=kp3OaLl-OHw <p/>
https://www.youtube.com/watch?v=ig9G36-8vJA <p/>
https://www.youtube.com/watch?v=FiueORvj7Kk <p/>

https://www.youtube.com/watch?v=v56zLOqhjV4 <p/>
https://www.youtube.com/watch?v=j_1JFnwOFwI <p/>
https://www.youtube.com/watch?v=USIqNZoDfNo <p/>
https://www.youtube.com/watch?v=e2YtARzJTys <p/>


### How To instructions
http://www.instructables.com/id/beet-box-raspberry-pi-style/ <p/>
http://www.instructables.com/id/Raspberry-Pi-Smart-Mirror/ <p/>
http://www.instructables.com/id/Amazon-Echo-on-Raspberry-Pi/ <p/>

### Classes
[Sensehat marble maze](https://www.raspberrypi.org/learning/sense-hat-marble-maze/worksheet/) - [sample](./mazepi.py)



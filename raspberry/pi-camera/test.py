
import json
import requests
from io import BytesIO
from time import sleep
from picamera import PiCamera

my_stream = open ('testimage.jpg', 'wb')
camera = PiCamera ()
camera.resolution = (1200, 1200)
#Camera Warm Up Time
sleep (2)
print "Capturing Image"
camera.rotation = 180
camera.start_preview ()
camera.capture(my_stream)
my_stream.close()



# sudo pip install keyboard
from sense_hat import SenseHat
sense = SenseHat()
import keyboard
import sys
g=[0,255,0]
e=[0,0,0]

arrow_up=[
 e,e,e,g,g,e,e,e,
 e,e,g,g,g,g,e,e,
 e,g,e,g,g,e,g,e,
 g,e,e,g,g,e,e,g,
 e,e,e,g,g,e,e,e,
 e,e,e,g,g,e,e,e,
 e,e,e,g,g,e,e,e,
 e,e,e,g,g,e,e,e,
]


while True:
 try:
  event = sense.stick.wait_for_event()
  print("The joystick was {} {}".format(event.action, event.direction))
  if (event.direction == "up"):
   keyboard.press_and_release('up')
 except KeyboardInterrupt:
        print("Ctrl-C Terminating...")
        sense.clear()
	sys.exit(0)
